# -*- encoding : utf-8 -*-
class SiteHooks < Spree::ThemeSupport::HookListener

  insert_after :inside_head, 'shared/styles'

end

