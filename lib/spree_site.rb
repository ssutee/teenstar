# -*- encoding : utf-8 -*-
require 'site_hooks'


module SpreeSite
  class Engine < Rails::Engine
    def self.activate
      # Add your custom site logic here
      ExternalGateway.send(:preference, :psb, :string)
      ExternalGateway.send(:preference, :biz, :string)
    
      if Spree::Config.instance
        Spree::Config.set(:site_name => "TeenStar")
        Spree::Config.set(:logo => 'teenstar_logo.png')
        Spree::Config.set(:mobile_logo => 'teenstar_mobile_logo.png')
        Spree::Config.set(:hd_logo => 'teenstar_hd_logo.png')
        Spree::Config.set(:allow_ssl_in_production => false)
        Spree::Config.set(:products_per_page => 1000)
        Spree::Config.set(:admin_products_per_page => 1000)        
        Spree::Config.set(:checkout_zone => 'Thailand')
        Spree::Config.set(:default_locale => 'th')
        Spree::Config.set(:default_country_id => 200)
        Spree::Config.set(:dummy_addresses => true)
        Spree::Config.set(:print_invoice_logo_path => "#{RAILS_ROOT}/public/images/teenstar_logo.png")
        Spree::Config.set(:print_invoice_font_name => "THSarabunNew")
        Spree::Config.set(:print_invoice_font_normal => "#{RAILS_ROOT}/public/fonts/THSarabunNew.ttf")
        Spree::Config.set(:print_invoice_font_bold => "#{RAILS_ROOT}/public/fonts/THSarabunNew Bold.ttf")
        Spree::Config.set(:print_buttons => "invoice,address")
        Spree::Config.set(:disable_bill_address => true)
        Spree::Config.set(:allow_guest_checkout => false)
        Spree::Config.set(:allow_backorder_shipping => true)
        Spree::Config.set(:show_zero_stock_products => true)
        Spree::Config.set(:allow_backorders => true)
        Spree::Config.set(:contact_email => 'admin@teenstar.in.th')
      end

      Dir.glob(File.join(File.dirname(__FILE__), "../app/**/*_decorator*.rb")) do |c|
        Rails.configuration.cache_classes ? require(c) : load(c)
      end

    end
    
    def load_tasks
    end
    
    config.to_prepare &method(:activate).to_proc
  

  end
end

