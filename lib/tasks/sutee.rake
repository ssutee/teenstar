require "yaml"

namespace :db do

  desc "Renew data"
  task :renew do
    #Rake::Task['site:install'].invoke
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].invoke
    Rake::Task['db:migrate'].invoke
    Rake::Task['db:seed'].invoke
    Rake::Task['db:admin:create'].invoke
    Rake::Task['spree:extensions:product_translations:globalize_legacy_data'].invoke
  end
  
  task :spree_globalize => :environment do
    unless ENV.include?("locale")
      raise "usage: rake db:spree_globalize locale= # a locale code" 
    end

    I18n.locale = ENV['locale'].to_sym

    @sql = ActiveRecord::Base.connection

    # wrapper to deal with differences in result sets from mysql, sqlite and postgres
    def fetch_first_row(query)
      result = @sql.execute(query)
      row = if defined?(PGresult) && result.is_a?(PGresult) #postgres
              result.getvalue(0,0)
            elsif result.is_a?(Array) #sqlite
              result[0][0]
            elsif defined?(Mysql2::Result) && result.is_a?(Mysql2::Result) #mysql2
              result.first.first
            else #mysql
              result.fetch_row.first
            end
    end

    puts "locale: #{I18n.locale}"
    puts "updating product names, description, meta_keywords and meta_description..."
    Product.all.each do |p|

      p.name = fetch_first_row("select products.name from products where products.id=#{p.id}")
      p.description = fetch_first_row("select products.description from products where products.id=#{p.id}")
      p.meta_keywords = fetch_first_row("select products.meta_keywords from products where products.id=#{p.id}")
      p.meta_description = fetch_first_row("select products.meta_description from products where products.id=#{p.id}")
      p.save!
    end
    puts "done."

    puts "updating taxonomy names..."
    Taxonomy.all.each do |t|
      t.name = fetch_first_row("select taxonomies.name from taxonomies where taxonomies.id=#{t.id}")
      t.save!
    end
    puts "done."

    puts "updating taxon names and permalinks..."
    Taxon.all.each do |t|
      t.name = fetch_first_row("select taxons.name from taxons where taxons.id=#{t.id}")
      t.save!
    end
    puts "done."

    puts "updating property presentations..."
    Property.all.each do |p|
      p.presentation = fetch_first_row("select properties.presentation from properties where properties.id=#{p.id}")
      p.save!
    end
    puts "done."

    puts "updating prototype names..."
    Prototype.all.each do |p|
      p.name = fetch_first_row("select prototypes.name from prototypes where prototypes.id=#{p.id}")
      p.save!
    end
    puts "done."

    puts "updating option type presentations..."

    OptionType.all.each do |p|
      p.presentation = fetch_first_row("select option_types.presentation from option_types where option_types.id=#{p.id}")
      p.save!
    end
    puts "done."

    OptionValue.all.each do |p|
      p.presentation = fetch_first_row("select option_values.presentation from option_values where option_values.id=#{p.id}")
      p.save!
    end
    puts "done."    
  end
  
end

namespace :mydb do
  desc "Dump schema and data to db/schema.rb and db/data.yml"
  task(:dump => [ "db:schema:dump", "db:data:dump" ])

  desc "Load schema and data from db/schema.rb and db/data.yml"
  task(:load => [ "db:schema:load", "db:data:load" ])

  namespace :data do
    def db_dump_data_file (extension = "yml")
      "#{dump_dir}/data.#{extension}"
    end

    def dump_dir(dir = "")
      "#{Rails.root}/db#{dir}"
    end

    desc "Dump contents of database to db/data.extension (defaults to yaml)"
    task :dump => :environment do
      format_class = ENV['class'] || "YamlDb::Helper"
      helper = format_class.constantize
      YAML::ENGINE.yamler = "psych"
      SerializationHelper::Base.new(helper).dump db_dump_data_file helper.extension
      YAML::ENGINE.yamler = "syck"
    end

    desc "Dump contents of database to curr_dir_name/tablename.extension (defaults to yaml)"
    task :dump_dir => :environment do
      format_class = ENV['class'] || "YamlDb::Helper"
      dir = ENV['dir'] || "#{Time.now.to_s.gsub(/ /, '_')}"
      YAML::ENGINE.yamler = "psych"      
      SerializationHelper::Base.new(format_class.constantize).dump_to_dir dump_dir("/#{dir}")
      YAML::ENGINE.yamler = "syck"      
    end

    desc "Load contents of db/data.extension (defaults to yaml) into database"
    task :load => :environment do
      format_class = ENV['class'] || "YamlDb::Helper"
      helper = format_class.constantize
      YAML::ENGINE.yamler = "psych"      
      SerializationHelper::Base.new(helper).load (db_dump_data_file helper.extension)
      YAML::ENGINE.yamler = "syck"      
    end

    desc "Load contents of db/data_dir into database"
    task :load_dir  => :environment do
      dir = ENV['dir'] || "base"
      format_class = ENV['class'] || "YamlDb::Helper"
      YAML::ENGINE.yamler = "psych"
      SerializationHelper::Base.new(format_class.constantize).load_from_dir dump_dir("/#{dir}")
      YAML::ENGINE.yamler = "syck"      
    end
  end
end
