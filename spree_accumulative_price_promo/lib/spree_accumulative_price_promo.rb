require 'spree_core'
require 'spree_accumulative_price_promo_hooks'

module SpreeAccumulativePricePromo
  class Engine < Rails::Engine

    def self.activate
      if File.basename( $0 ) != "rake"
        # register promotion rules
        [Promotion::Rules::AccumulativePrice].each &:register
      end
    end
    
    config.autoload_paths += %W(#{config.root}/lib)
    config.to_prepare &method(:activate).to_proc
  end
end
