class SpreeAccumulativePricePromoHooks < Spree::ThemeSupport::HookListener
  insert_after :account_my_orders, :partial => 'users/promotions'
end