class Promotion::Rules::AccumulativePrice < PromotionRule
  
  preference :amount, :decimal, :default => 1000.00
  preference :operator, :string, :default => '>'

  OPERATORS = ['gt', 'gte']
    
  def eligible?(order)
    accumulative_price = order.user.orders.select{|o| o.payment_state == "paid"}.collect{|o| o.item_total}.inject{|sum,x| sum+x}
    accumulative_price.nil? ? false : accumulative_price.send(preferred_operator == 'gte' ? :>= : :>, preferred_amount)
  end
end