# -*- encoding : utf-8 -*-
Teenstar::Application.routes.draw do

  root :to => "pages#index"

  match 'products', :to => 'products#index', :as => "products"  
  #match 'promotions', :to => 'products#promotions', :as => "products"

  namespace :admin do
    resources :reports, :only => [:index, :show] do
      collection do
        get :safety_stock
        get :sales_total
      end
    end
  end
    
end
