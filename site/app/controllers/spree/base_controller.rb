class Spree::BaseController < ActionController::Base
  include SpreeBase
  include SpreeRespondWith
  
  before_filter { Impression.create(:ip_address => request.remote_ip) }
  
  def get_taxonomies
    @taxonomies ||= Taxonomy.find(:all, :include => {:root => :children})	  	
  	@taxonomies.reject { |t| t.root.nil? }
  end
end
