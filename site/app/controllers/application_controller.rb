# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery
  
  include SessionsHelper
  
  before_filter :log_impression
  before_filter :set_locale
  
  def log_impression
    Impression.create(:ip_address => request.remote_ip)
  end
  
  def set_locale
    I18n.locale = params[:locale] if params.include?('locale')
  end

  def default_url_options(options = {})
    options.merge!({ :locale => I18n.locale })
  end
  
  # Customize the Devise after_sign_in_path_for() for redirecct to previous page after login
  def after_sign_in_path_for(resource_or_scope)
    case resource_or_scope
    when :user, User
      previous_location = session[:return_to]
      clear_stored_location
      (previous_location.nil?) ? root_path : previous_location.to_s
    else
      super
    end
  end
  
end

