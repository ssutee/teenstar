Image.class_eval do
  attachment_definitions[:attachment][:styles] = {
    :mini => '80x80#', # thumbs under image
    :small => '160x160#', # images on category view
    :product => '339x339>', # full product image
    :large => '1600x1067#',  # full-screen box image
    :lightbox => '600x400#', # lightbox image
    :carousel => '650x316#', # carousel image
    :collection => '183x183#', # collection image
  }
end
