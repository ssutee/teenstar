require 'prawn/layout'

font_name = Spree::Config[:print_invoice_font_name] 
font_normal = Spree::Config[:print_invoice_font_normal] 
font_bold = Spree::Config[:print_invoice_font_bold]
teenstar_logo = Spree::Config[:print_invoice_logo_path]

if !font_normal.nil? && !font_bold.nil?
  font_families.update(
    font_name => {:normal => font_normal,
                  :bold => font_bold})
end

font_name ||= "Helvetica"

font font_name, :size => 20

bounding_box [10,680], :width => 540 do

  pdf.image teenstar_logo, :at => [390, 30], :width => 150

  text I18n.t(:order) + " " + @order.number, :align => :left, :size => 24, :style => :bold
  
  move_down 35
  
  data = [[Prawn::Table::Cell.new( :text => I18n.t(:item_description), :font_style => :bold, :border_width => 1, :borders => [:bottom]),
           Prawn::Table::Cell.new( :text => I18n.t(:price), :font_style => :bold, :border_width => 1, :borders => [:bottom] ),
           Prawn::Table::Cell.new( :text => I18n.t(:qty), :font_style => :bold, :border_width => 1, :borders => [:bottom] ),
           Prawn::Table::Cell.new( :text => I18n.t(:total), :font_style => :bold, :border_width => 1, :borders => [:bottom] )]]

  @order.line_items.each do |item|
    variant = "(" + variant_options(item.variant) + ")" unless item.variant.option_values.empty?
    variant ||= ""
    data << [item.variant.product.name + variant, 
             number_to_currency(item.price), 
             item.quantity, 
             number_to_currency(item.price * item.quantity)]
    data << [I18n.t(:subtotal), " ", " ", number_to_currency(@order.item_total)]
    
    @order.adjustments.each do |adjustment|
      data << [adjustment.label, " ", " ", adjustment.amount >= 0 ? number_to_currency(adjustment.amount): "-#{number_to_currency(adjustment.amount.abs)}"]
    end
     
    data << [I18n.t(:order_total), " ", " ", number_to_currency(@order.total)]
    
    data << [Prawn::Table::Cell.new( :text => " ", :border_width => 1, :borders => [:bottom]),
             Prawn::Table::Cell.new( :text => " ", :border_width => 1, :borders => [:bottom] ),
             Prawn::Table::Cell.new( :text => " ", :border_width => 1, :borders => [:bottom] ),
             Prawn::Table::Cell.new( :text => " ", :border_width => 1, :borders => [:bottom] )]
    
  end
  
  table data,
   :position => :center,
   :border_width => 0.5,
   :header => true,
   :vertical_padding   => 10,
   :horizontal_padding => 6,
   :font_size => 16,
   :border_style => :underline_header,
   :column_widths => { 0 => 260, 1 => 105 , 2=> 50, 3 => 105}  
  
  move_down 10
  text I18n.t(:how_to_pay), :align => :left, :size => 20, :style => :bold
  move_down 10
  text I18n.t(:transfer_to), :align => :left, :size => 20
  move_down 5
  text I18n.t(:transfer_bank), :align => :left, :size => 20
  move_down 5
  text I18n.t(:call_center), :align => :left, :size => 20

end