# -*- encoding : utf-8 -*-
require 'spree_print_order_hooks'
require 'prawn_handler'

module SpreePrintOrder
  class Engine < Rails::Engine

    def self.activate
      OrdersController.class_eval do
        respond_to :html, :pdf
        respond_override(:show => {:pdf => {:success =>
            lambda { render :layout => false } } })
      end
      
    end

    config.autoload_paths += %W(#{config.root}/lib)
    config.to_prepare &method(:activate).to_proc
  end
end

Mime::Type.register 'application/pdf', :pdf

