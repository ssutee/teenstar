# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  # Add your extension routes here
  namespace :gateway do
    match '/:gateway_id/paysbuy/:order_id' => 'paysbuy#show',     :as => :paysbuy
    match '/paysbuy/:id/comeback'          => 'paysbuy#comeback', :as => :paysbuy_comeback
  end
end
