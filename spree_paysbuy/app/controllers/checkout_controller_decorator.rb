# -*- encoding : utf-8 -*-
CheckoutController.class_eval do

  before_filter :redirect_for_paysbuy, :only => :update

  private

  def redirect_for_paysbuy
    return unless params[:state] == "payment"
    @payment_method = PaymentMethod.find(params[:order][:payments_attributes].first[:payment_method_id])
    if @payment_method && @payment_method.kind_of?(PaymentMethod::Paysbuy)
      if @order.update_attributes(object_params)
        if @order.next
          state_callback(:after)
          redirect_to gateway_paysbuy_path(:gateway_id => @payment_method.id, :order_id => @order.id)
        else
          flash[:error] = I18n.t(:payment_processing_failed)
          respond_with(@order, :location => checkout_state_path(@order.state))
          return
        end
      else
        respond_with(@order) { |format| format.html { render :edit } }
      end
    end
  end

end
