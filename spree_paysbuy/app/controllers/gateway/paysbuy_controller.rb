# -*- encoding : utf-8 -*-
class Gateway::PaysbuyController < Spree::BaseController
  include ERB::Util
  skip_before_filter :verify_authenticity_token, :only => [:comeback]
  
  def show
    @order = Order.find(params[:order_id])
    @gateway = @order.available_payment_methods.find{|x| x.id == params[:gateway_id].to_i }

    #@order.payments.destroy_all    
    #payment = @order.payments.create!(:amount => 0, :payment_method_id => @gateway.id)

    if @order.blank? || @gateway.blank?
      flash[:error] = I18n.t("invalid_arguments")
      redirect_to :back
    else
      @bill_address, @ship_address = @order.bill_address, (@order.ship_address || @order.bill_address)
      render :action => :show
    end
  end

  def comeback
    @order = Order.find_by_number(params[:id])

    @gateway = @order && @order.payments.first.payment_method
    if @gateway && @gateway.kind_of?(PaymentMethod::Paysbuy) && params[:DR] && params[:result] != "99#{params[:id]}"
        
      @order.next
      @order.save

      @order.build_paysbuyinfo({ :first_name => @order.bill_address.firstname,
                                 :last_name => @order.bill_address.lastname,
                                 :result => params[:result],
                                 :apcode => params[:apcode],
                                 :amt => params[:amt],     
                                 :method => params[:method],                                                                                                                  
                                 :fee => params[:fee] })
      @order.save
      
      session[:order_id] = nil

      redirect_to order_url(@order, {:checkout_complete => true, :order_token => @order.token}), 
                            :notice => I18n.t("payment_success")
    else
      flash[:error] = I18n.t("paysbuy_payment_response_error")
      redirect_to (@order.blank? ? root_url : edit_order_url(@order))
    end
  end
end
