# -*- encoding : utf-8 -*-
class PaymentMethod::Paysbuy < PaymentMethod
    
  preference :biz, :string, :default => "demo@paysbuy.com"
  preference :psb, :string, :default => "psb"
  preference :server, :string, :default => "http://demo.paysbuy.com/paynow.aspx"
  preference :currency_code, :string, :default => "764"
  preference :post_url, :string
  preference :opt_fix_method, :string, :default => "1"
  preference :opt_fix_redirect, :string

  def payment_profiles_supported?
    false
  end

  def actions
    %w{capture void}
  end
  
  # Indicates whether its possible to capture the payment
  def can_capture?(payment)
    ['checkout', 'pending'].include?(payment.state)
  end
  
  # Indicates whether its possible to void the payment.
  def can_void?(payment)
    payment.state != 'void'
  end
  
  def capture(payment)
    payment.update_attribute(:state, 'pending') if payment.state == 'checkout'
    payment.complete
    true
  end
  
  def void(payment)
    payment.update_attribute(:state, 'pending') if payment.state == 'checkout'
    payment.void
    true
  end

end
