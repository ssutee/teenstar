# -*- encoding : utf-8 -*-
# RVM bootstrap
#$:.unshift(File.expand_path("~/.rvm/lib"))
set :rvm_ruby_string, '1.9.2-p290'
# set :rvm_ruby_string, 'ree@rails3'

# def disable_rvm_shell(&block)
#   old_shell = self[:default_shell]
#   self[:default_shell] = nil
#   yield
#   self[:default_shell] = old_shell
# end

set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"")

require 'rvm/capistrano'
set :rvm_type, :user

# bundler bootstrap
require 'bundler/capistrano'

#main detail

ssh_options[:port] = 60000

set :application, "116.251.223.178"
role :web, "116.251.223.178"                          # Your HTTP server, Apache/etc
role :app, "116.251.223.178"                          # This may be the same as your `Web` server
role :db,  "116.251.223.178", :primary => true # This is where Rails migrations will run

# server details

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
set :deploy_to, "/home/sutee/teenstar"
set :deploy_via, :remote_cache
set :user, "sutee"
set :use_sudo, false

# repo details
set :scm, :git
# set :scm_username, "gitosis"
set :repository, "git@bitbucket.org:ssutee/teenstar.git"
set :branch, "master"
set :git_enable_submodules, 1

# disable_rvm_shell do
#   run "rvm install 1.9.2"
#   run "rvm use 1.9.2@MY_APP --create"
# end

#tasks
namespace :deploy do
  task :start, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

  task :stop, :roles => :app do
    # Do nothing.
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
  end

  desc "Symlink shared resources on each release"
  task :symlink_shared, :roles => :app do
    #run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end

after 'deploy:update_code', 'deploy:symlink_shared'

# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`


# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
