require 'delayed_job'
Delayed::Worker.backend = :active_record
Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay = 3
Delayed::Worker.max_attempts = 20
Delayed::Worker.max_run_time = 30.minutes
Delayed::Worker.delay_jobs = !Rails.env.test?