# -*- encoding : utf-8 -*-
class CreatePaysbuyinfos < ActiveRecord::Migration
  def self.up
    create_table :paysbuyinfos do |t|
      t.string :first_name
      t.string :last_name
      t.string :result
      t.string :apcode
      t.string :amt
      t.string :fee
      t.string :method
      t.integer :order_id

      t.timestamps
    end
  end

  def self.down
    drop_table :paysbuyinfos
  end
end
