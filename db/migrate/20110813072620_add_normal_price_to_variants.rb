class AddNormalPriceToVariants < ActiveRecord::Migration
  def self.up
    add_column :variants, :normal_price, :float, :default => 0, :null => false
  end

  def self.down
    remove_column :variants, :normal_price
  end
end
