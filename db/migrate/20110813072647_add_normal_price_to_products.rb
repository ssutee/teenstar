class AddNormalPriceToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :normal_price, :float, :default => 0, :null => false
  end

  def self.down
    remove_column :products, :normal_price
  end
end
